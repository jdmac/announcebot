package main

import (
	"announceBot/config"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

type structuredResponse struct {
	Data []struct {
		UserName string `json:"User_Name"`
	}
}

//var TrackingList = map[string]bool{}
//var NewTrackingList = map[string]bool{}

func main() {
	conf := config.LoadConfig()

	if err := pollApi(conf); err != nil {
		log.Fatal(err)
	}
}

func pollApi(conf *config.Configuration) error {
	url := "https://api.twitch.tv/helix/streams?user_login=" + conf.GenUserLoginQuery()

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	req.Header.Add("Client-ID", conf.ClientID)

	client := &http.Client{Timeout: time.Second * 10}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	data := &structuredResponse{}

	err = json.NewDecoder(resp.Body).Decode(data)
	if err != nil {
		return err
	}

	err = processData(data)
	if err != nil {
		return err
	}

	return nil
}

func processData(data *structuredResponse) error {
	for _, stream := range data.Data {
		fmt.Println(stream.UserName)
	}
	return nil
}
