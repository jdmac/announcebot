package config

func (c *Configuration) GenUserLoginQuery() string {
	userQuery := ""

	for i := 0; i < len(c.StreamUsers); i++ {
		userQuery = userQuery + c.StreamUsers[i] + "&"

		if (i + 1) == len(c.StreamUsers) {
			userQuery = userQuery[:len(userQuery)-1]
		}
	}

	return userQuery
}
