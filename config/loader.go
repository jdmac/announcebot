package config

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"strings"
)

func LoadConfig() *Configuration {
	file, _ := os.Open("config.json")

	var config = &Configuration{}

	err := json.NewDecoder(file).Decode(config)

	if err != nil {
		log.Fatal(err)
	}

	err = parseUsers(config)

	if err != nil {
		log.Fatal(err)
	}

	return config
}

func parseUsers(c *Configuration) error {

	if len(c.StreamUsers) == 0 {
		return errors.New("no stream users found")
	}

	for i := 0; i < len(c.StreamUsers); i++ {
		if StreamUsers[strings.ToLower(c.StreamUsers[i])] {
			c.StreamUsers = append(c.StreamUsers[:i], c.StreamUsers[i+1:]...)
		} else {
			StreamUsers[strings.ToLower(c.StreamUsers[i])] = true
		}
	}

	// Delete
	StreamUsers = nil

	return nil
}