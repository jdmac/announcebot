package config

type Configuration struct {
	ClientID     string
	StreamUsers  []string
	DiscordHooks []map[string]string
}

var StreamUsers = map[string]bool{}
